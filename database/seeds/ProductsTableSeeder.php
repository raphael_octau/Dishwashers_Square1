<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the products seeds
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Montpellier 12 Place Settings White Freestanding 12L Dishwasher DW1254P',
            'mark' => 'Montpellier',
            'price' => 219.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzE0MDZcXFwvNTk4MmZlNDQ4ZTNmZS1hZDhlMGRiMjY2OTMzYmRkYWFlZmI5MjFjMDgzMjU3NFwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiZTlkN2NmMTFkNzE0MWE2YjMzZDA3OWNkMzY2ZjVkOGQ4OWJmNzFjNiJ9/montpellier-12-place-settings-white-freestanding-12l-dishwasher-dw1254p',
			'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
        DB::table('products')->insert([
            'name' => 'Beko 12 Place Setting White Freestanding 12L Dishwasher DFN04210W',
            'mark' => 'Beko',
            'price' => 239.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzI3NTVcXFwvNTk4YzVlYTkwM2M5Ny04NTIzMzJkNjMwZTdkYWVjMWVlZjVkNmY4YjFhNTEyYlwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiMjYxMTJhMDYwMzU5NzM0ZmRhZGMwMmNmOGI1YWY1OWVhNTlkYWU1OSJ9/beko-12-place-setting-white-freestanding-12l-dishwasher-dfn04210w',
        	'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
        DB::table('products')->insert([
            'name' => 'Montpellier 10 Place Setting 11L White Freestanding Slimline Dishwasher DW1064P-2',
            'mark' => 'Montpellier',
            'price' => 249.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzM3NThcXFwvNWFkNzM3ZDAwYWI4MC00OWRjZGNjNjViNzI4MWExZDc4YWJkZGIyNDAxMTdkZFwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiZDIyOWM1ODg3NjhiMDI0MDgzNTU5OTVjMjVhNTkxYzBlY2ZjOTcwOCJ9/montpellier-10-place-setting-11l-white-freestanding-slimline-dishwasher-dw1064p-2',
        	'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
        DB::table('products')->insert([
            'name' => 'Montpellier 12 Place Setting Silver Freestanding 12L Dishwasher DW1254S',
            'mark' => 'Montpellier',
            'price' => 261.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzE0MThcXFwvNTk0MjQ4ZmRkZGRmNy1mMTQ5YmEzMTE3MTZhZTYxYTY3MTM1MjczMmUxNDUwZFwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiZTg3ZDdjZDdmOTU3ODU0ZTA1ZGE4YzY3ZTFjZmRmNGQxMGM2NTYzNyJ9/montpellier-12-place-setting-silver-freestanding-12l-dishwasher-dw1254s',
        	'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
        DB::table('products')->insert([
            'name' => 'Hotpoint Aquarius 10 Place Setting White Freestanding Slimline 10L Dishwasher SIAL11010P',
            'mark' => 'Hotpoint',
            'price' => 269.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzIyMTVcXFwvNTk4YzYwYTlkNDdkOS0xZTdjMjBmYmUwZTQ0ZTI5OWEyYzA4YjczZGIwM2UyOFwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiNjE5MWI0ZTYwN2VmMjA3ZmExNjg4YzQ4NDBiNGUxZjA2MzZkYmQ0YiJ9/hotpoint-aquarius-10-place-setting-white-freestanding-slimline-10l-dishwasher-sial11010p',
        	'created_at' => Carbon::now(),
			'updated_at' => Carbon::now(),
		]);
        DB::table('products')->insert([
            'name' => 'Montpellier 12 Place Settings White Integrated Dishwasher MDI600',
            'mark' => 'Montpellier',
            'price' => 283.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzE0MzBcXFwvOGRmN2I3M2E3ODIwZjRhZWY0Nzg2NGYyYTZjNWZjY2ZcIixcIndpZHRoXCI6MjUwLFwiaGVpZ2h0XCI6XCJcIixcImRlZmF1bHRcIjpcImh0dHBzOlxcXC9cXFwvczMtZXUtd2VzdC0xLmFtYXpvbmF3cy5jb21cXFwvc3RvcmFnZS5idXlhbmRzZWxsLmllXFxcL2FwcGxpYW5jZXMtZGVsaXZlcmVkLW5vaW1hZ2UucG5nXCJ9IiwiaGFzaCI6ImQwMzYzYmY0NzJlOTIwYTNhZDYzYWQzZWI5NmNhZWNlMGU2ZDZjMzgifQ==/montpellier-12-place-settings-white-integrated-dishwasher-mdi600',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Amica 12 Place Setting White Freestanding 11L Dishwasher ZWM696W',
            'mark' => 'Amica',
            'price' => 289.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzE1MDRcXFwvNTk1YmI4NzA1YTY4Zi0yNzM5ZjEzMGZkZWRmYmIyOTlkOTAwZjcyMzFhZDdjOFwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiZDMwMjEzZGMyODNhMWQ3OTAwNmM1NjFkNGQ4MDFiMGI3YzU4YjQwMSJ9/amica-12-place-setting-white-freestanding-11l-dishwasher-zwm696w',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Montpellier 12 Place Setting Black Freestanding 12L Dishwasher DW1254K',
            'mark' => 'Montpellier',
            'price' => 289.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzE1NzlcXFwvNTlkYjk0MjU5ZjdlZC04YjY3MzY4NzY4ZmY2Y2JiZGJiNjQ1ZDA3ODkyMjI2YVwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiYzM1YWRhODcwNjc2OWVjNDViOGQxNWE0Mjg3YjBhNmEyM2UyYjA1ZiJ9/montpellier-12-place-setting-black-freestanding-12l-dishwasher-dw1254k',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Sharp 12 Place Settings White Freestanding Dishwasher QWF471W',
            'mark' => 'Sharp',
            'price' => 299.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzM4NDZcXFwvNWFlNzE4YTAzYzJjZC00NjUxZTgwYThhY2YwNmU4YzRmOTc1MGZkOWQxNmZhNVwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiOGE3MzdiZDQ2MDYzZmY5MmE5NzhlOTI3YmFlYmYyZTc3NjZmNmIzNyJ9/sharp-12-place-settings-white-freestanding-dishwasher-qwf471w',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Montpellier 12 Place Settings White Semi-Integrated Dishwasher MDI650W',
            'mark' => 'Montpellier',
            'price' => 319.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzMwNzFcXFwvNTliZmM3NjJjN2JhYi0zMTAwYTgwNTlmZjU1NTBiYWQ1YTBlNTMwNWM2ZTZlMVwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiNTI0ZDlkODg4MTVkMDQ1ZDE0NTkzNjZmMTA2NDM4ZGUxYzUzNThjZSJ9/montpellier-12-place-settings-white-semi-integrated-dishwasher-mdi650w',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Culina 9 Place Settings Integrated Slimline Dishwasher UBMIDW45',
            'mark' => 'Culina',
            'price' => 329.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzE5OTRcXFwvNTdkODAyYTkwZTcyYS0zMmYxMGY1OWEyYmVjMmY4YTc0NDJhNDljNjBlOTAxM1wiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiOGQ4NzJhYmRjOWJhZWIzOTU1MjIzNWM2YzhlYTI1YWQyMjRlYTgzOSJ9/culina-9-place-settings-integrated-slimline-dishwasher-ubmidw45',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Amica 14 Place Setting Integrated 10L Dishwasher ZIM688E',
            'mark' => 'Amica',
            'price' => 339.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzE1MDNcXFwvNTk5ZWJkZWUzNjAzMy1jYzQzNDM3ODEzMzU3ODVjMTYyYzUzNjA3NWQyYTMxMlwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiNDg1YjU5ZjM2ZDgwNzMyZjY3N2E5MTlkOWUzYmQxZDAzMGU2NzNjYSJ9/amica-14-place-setting-integrated-10l-dishwasher-zim688e',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Montpellier 12 Place Settings White Integrated 12L Dishwasher MDI700',
            'mark' => 'Montpellier',
            'price' => 339.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzE2MjlcXFwvMDNiODRjYjk2NGVjOWY2YWMzNTUyNjViN2VlNzVjYTZcIixcIndpZHRoXCI6MjUwLFwiaGVpZ2h0XCI6XCJcIixcImRlZmF1bHRcIjpcImh0dHBzOlxcXC9cXFwvczMtZXUtd2VzdC0xLmFtYXpvbmF3cy5jb21cXFwvc3RvcmFnZS5idXlhbmRzZWxsLmllXFxcL2FwcGxpYW5jZXMtZGVsaXZlcmVkLW5vaW1hZ2UucG5nXCJ9IiwiaGFzaCI6IjQyMGFhMzY2NTc2NDI5OTcxOTJmZjUzNjI2NTY0MDRjYzkwZmFkMjYifQ==/montpellier-12-place-settings-white-integrated-12l-dishwasher-mdi700',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Whirlpool 10 Place Settings White Freestanding Slimline 10L Dishwasher ADP301WH',
            'mark' => 'Whirlpool',
            'price' => 349.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzMwNzRcXFwvNTljMGQ0MTFlMDU4Zi03ZTE3NjQ5NWQwZDhkYjU2Yjc4OWRjNjRhZDljM2E2YlwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiZjE0NmI3ZWZkOTQ4NjEwZWUyM2ZiZmQyNTlkZjUyNmE5MGE4OGJkMSJ9/whirlpool-10-place-settings-white-freestanding-slimline-10l-dishwasher-adp301wh',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Sharp 12 Place Settings White Freestanding Dishwasher QWG472W',
            'mark' => 'Sharp',
            'price' => 349.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzM4NDdcXFwvNWFlNzFhMmI0YjlhMy1iZWUyMjlhZGFiMzc2NDZiZDQ5MDdkYjYzNTBlNTEwZVwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiNmJhMWExYmVlMzE5M2YzMTcxYjY0YmVkYzAwYzk0NjRhZjM5OWRhNSJ9/sharp-12-place-settings-white-freestanding-dishwasher-qwg472w',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Montpellier 12 Place Setting White Semi-Integrated 9L Dishwasher MDI650X',
            'mark' => 'Montpellier',
            'price' => 352.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzE2MzBcXFwvMDNiODRjYjk2NGVjOWY2YWMzNTUyNjViN2VlNzVjYTZcIixcIndpZHRoXCI6MjUwLFwiaGVpZ2h0XCI6XCJcIixcImRlZmF1bHRcIjpcImh0dHBzOlxcXC9cXFwvczMtZXUtd2VzdC0xLmFtYXpvbmF3cy5jb21cXFwvc3RvcmFnZS5idXlhbmRzZWxsLmllXFxcL2FwcGxpYW5jZXMtZGVsaXZlcmVkLW5vaW1hZ2UucG5nXCJ9IiwiaGFzaCI6ImZhM2Q1MjU1NzdlZDk0YTcxNWEwNTUzYzNiZThmMWQ3OGM5YTk1ZDQifQ==/montpellier-12-place-setting-white-semi-integrated-9l-dishwasher-mdi650x',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Indesit 10 Place Settings Integrated Slimline Dishwasher DISR14B1',
            'mark' => 'Indesit',
            'price' => 354.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzE1NjBcXFwvZjIwNDhhODNiZjhjZjI3ZDNiZGY1YjJkN2VlZWZjMDdcIixcIndpZHRoXCI6MjUwLFwiaGVpZ2h0XCI6XCJcIixcImRlZmF1bHRcIjpcImh0dHBzOlxcXC9cXFwvczMtZXUtd2VzdC0xLmFtYXpvbmF3cy5jb21cXFwvc3RvcmFnZS5idXlhbmRzZWxsLmllXFxcL2FwcGxpYW5jZXMtZGVsaXZlcmVkLW5vaW1hZ2UucG5nXCJ9IiwiaGFzaCI6IjdjZjUwNzMzNmZjZDU2YTc5ZTNiNjAxNGY5M2EzNTdhNzc1ZGNhNmIifQ==/indesit-10-place-settings-integrated-slimline-dishwasher-disr14b1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Montpellier 15 Place Settings Integrated Dishwasher MDI800',
            'mark' => 'Montpellier',
            'price' => 368.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzE5MTZcXFwvNTk5ZWI0ZjcwMTEzYS04YWI2YWZhNzQ3MmI5Mzk0NTlkODdkNGZlYWVjODU3OFwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiY2MxMWY0ZmVjMmE1ZmFhNWUyNjUyNjRlZDE1ZDVkNGFhZDE1NzQwMSJ9/montpellier-15-place-settings-integrated-dishwasher-mdi800',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Whirlpool 10 Place Settings Stainless Steel Freestanding Slimline 10L Dishwasher ADP301IX',
            'mark' => 'Whirlpool',
            'price' => 369.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzI4NzJcXFwvNTk0YTdhMzU1NmE3NS03NTcyNzZmZDU2YWQ3ZGQyMjI3YzBlNmVjN2NjZjk0YlwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiOTc2MTczOTM1M2Q2MDA4ZjFhOTU2ZGNlOTRlYTlmOTJhYmRmOTllNSJ9/whirlpool-10-place-settings-stainless-steel-freestanding-slimline-10l-dishwasher-adp301ix',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Amica 15 Place Settings Integrated 12L Dishwasher ZIV635',
            'mark' => 'Amica',
            'price' => 369.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzIyODNcXFwvNTk1YmJkZTExM2I1YS1hNzkyMmVkYzJlMmRlOGQ1NGNiNDRmODQyZTUxZGNiMlwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiNjQ2MDBhNWI1MDE1ZTJjMTAyOTAyYzVjMDVmMjI4MWZkZGIxZjNjNCJ9/amica-15-place-settings-integrated-12l-dishwasher-ziv635',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Zanussi 9 Place Setting Integrated Slimline 9.5L Dishwasher ZDV12002FA',
            'mark' => 'Zanussi',
            'price' => 449.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzI0NDFcXFwvNTgzODE5YzNkNTFkMS01MDUxNDU3ZGViMDc5ZDNiZTUzZTEzZmU3Y2RlZWI1OFwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiZjVhZmVhZjY1MmY0Zjc5Y2I2MTVkMTI4NmE1YjhlMWE5YjcyYTAyYiJ9/zanussi-9-place-setting-integrated-slimline-9-5l-dishwasher-zdv12002fa',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Gorenje 12 Place Settings Integrated 12L Dishwasher GV60110UK',
            'mark' => 'Gorenje',
            'price' => 479.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzY2MFxcXC81OTViYzI2Mzg4N2ExLTk5OTdmMDJiMWE2YmVmNzU5ZmU0N2Y0ZmViNjJkMWI4XCIsXCJ3aWR0aFwiOjI1MCxcImhlaWdodFwiOlwiXCIsXCJkZWZhdWx0XCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC9hcHBsaWFuY2VzLWRlbGl2ZXJlZC1ub2ltYWdlLnBuZ1wifSIsImhhc2giOiJlY2QxYzA5Mzc0ODU3ODc5YmM3YzM0MDFmODBhYzczOGMzODU1YWM0In0=/gorenje-12-place-settings-integrated-12l-dishwasher-gv60110uk',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Samsung 9 Place Settings Integrated Slimline Dishwasher DW50K4050BB/EU',
            'mark' => 'Samsung',
            'price' => 479.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzMyNTRcXFwvNWEwYjBlNmE0ZDZkZi05MjQxNDRhNWQ2NzAyYjlmNmY5OTc5YThjNzNiY2YzMVwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiYjY5OTJlOGMzOWFhNzEyMmFkNWZhYTMzODQwNGZhNzdiMTJiNjUwZiJ9/samsung-9-place-settings-integrated-slimline-dishwasher-dw50k4050bb-eu',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Whirlpool 14 Place Settings Stainless Steel Freestanding Dishwasher WFC3C24PX',
            'mark' => 'Whirlpool',
            'price' => 499.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzI0NzVcXFwvNTg3ZGVkNjQ4OGQxNi0xNWE2ZjkxZGM0MWNhNzEyOGE5MjcyYjVlY2ZhOTFjY1wiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiNWFjZmM5NjY3MzQ3YjQ1OGUwMzM0NmM1MmExOWY3YmM4YzU0Nzc3MyJ9/whirlpool-14-place-settings-stainless-steel-freestanding-dishwasher-wfc3c24px',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Bosch 13 Place Settings White Freestanding 6.5L Dishwasher SMS46IW02G',
            'mark' => 'Bosch',
            'price' => 499.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzMwMzlcXFwvNTliMjVhMDVjZGVkNS0xNzQ5NmI1MjZhMTA4NmYzMTc0MjM1OThkMDZjOTg3N1wiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiY2Y3MTBlYjA1ODdiN2E3N2FiNDgwOGJlZmE5MzUxNTBiY2U2MWY3MiJ9/bosch-13-place-settings-white-freestanding-6-5l-dishwasher-sms46iw02g',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Samsung 13 Place Settings White Integrated Dishwasher DW60M5040BB/EU',
            'mark' => 'Samsung',
            'price' => 529.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzMyNTNcXFwvNWEwYjBlMWQxYTM0Mi1hYzBjOWZlNmE3ZjEzMmRkOWU4MzU4ZTlkYmEyNGE2NlwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiMDFhNjU0MmZjMDEzNTkyMDBhMTkwODhhNTc0YzRlMWNmNDM0OGJjNyJ9/samsung-13-place-settings-white-integrated-dishwasher-dw60m5040bb-eu',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Samsung 13 Place Settings Integrated Dishwasher DW60M6040BB/EU',
            'mark' => 'Samsung',
            'price' => 579.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzMyNTJcXFwvNWEwYjBkYmU3MGRhNi00NGQ2YTRmZDBkODNlN2M4MTRlMTFlOGQyZjdlODQwYVwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiMWYyNmJiYzZlNzcwOTYwOTBhMTg3NzUzNzJhMjI5ZmFmYWU0ZWQzMyJ9/samsung-13-place-settings-integrated-dishwasher-dw60m6040bb-eu',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Gorenje 14 Place Setting Integrated 10L Dishwasher GV63315UK',
            'mark' => 'Gorenje',
            'price' => 599.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzQyNDdcXFwvMWQxNTBlYzIxZGIzODFkMGI1ZjZlZjU1NWRhODFlM2JcIixcIndpZHRoXCI6MjUwLFwiaGVpZ2h0XCI6XCJcIixcImRlZmF1bHRcIjpcImh0dHBzOlxcXC9cXFwvczMtZXUtd2VzdC0xLmFtYXpvbmF3cy5jb21cXFwvc3RvcmFnZS5idXlhbmRzZWxsLmllXFxcL2FwcGxpYW5jZXMtZGVsaXZlcmVkLW5vaW1hZ2UucG5nXCJ9IiwiaGFzaCI6IjA0MTdlMDFlYzUyOWRlMDBmZDA3YzRkZTI4NjU0MGQ2MjA4YzJiMGYifQ==/gorenje-14-place-setting-integrated-10l-dishwasher-gv63315uk',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        DB::table('products')->insert([
            'name' => 'Samsung 14 Place Settings Integrated Dishwasher with Waterfall Technology DW60M9550BB/EU',
            'mark' => 'Samsung',
            'price' => 1299.95,
            'picture' => 'https://img.resized.co/appliancesdelivered/eyJkYXRhIjoie1widXJsXCI6XCJodHRwczpcXFwvXFxcL3MzLWV1LXdlc3QtMS5hbWF6b25hd3MuY29tXFxcL3N0b3JhZ2UuYnV5YW5kc2VsbC5pZVxcXC91cGxvYWRzXFxcLzMzNDdcXFwvNWEyN2I4Y2QxMmI1ZC1hNjgxM2U5MWQyMTQwZjg0ZWVlNTJhZGE2YzVlNjBjMFwiLFwid2lkdGhcIjoyNTAsXCJoZWlnaHRcIjpcIlwiLFwiZGVmYXVsdFwiOlwiaHR0cHM6XFxcL1xcXC9zMy1ldS13ZXN0LTEuYW1hem9uYXdzLmNvbVxcXC9zdG9yYWdlLmJ1eWFuZHNlbGwuaWVcXFwvYXBwbGlhbmNlcy1kZWxpdmVyZWQtbm9pbWFnZS5wbmdcIn0iLCJoYXNoIjoiYzBhNTQyOGM2NWI2MmNmNmI1NzE1YjVlODM1MjA3ZGIxOTc2MjdjNCJ9/samsung-14-place-settings-integrated-dishwasher-with-waterfall-technology-dw60m9550bb-eu',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
