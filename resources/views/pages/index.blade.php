@extends('layouts.default')

@section('content')

    <!-- Sorting buttons -->
    <div class="d-flex align-items-center mb-2">
        <h1 class="display-4 m-0 w-100 mr-2">@lang('messages.home')</h1>

        <div class="flex-shrink-1">
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                <!-- Sort by mark -->
                <label class="btn btn-secondary btn-sort">
                    <input type="radio" name="mark-asc" id="sort-alpha" autocomplete="off">
                    <i class="fas fa-sort-alpha-down mr-2"></i>@lang('messages.mark')
                </label>
                <!-- Sort by price -->
                <label class="btn btn-secondary btn-sort">
                    <input type="radio" name="price-asc" id="sort-numeric" autocomplete="off">
                    <i class="fas fa-sort-numeric-down mr-2"></i>@lang('messages.price')
                </label>
            </div>
        </div>
    </div>
    <!-- -- End Sorting buttons -->

    <!-- Products zone -->
    <div class="row">

        @foreach($products as $product)

            <div class="col-sm-12 col-md-6 col-lg-4 mb-4">

                <div class="card shadow-sm">
                    @if($product->in_wishlist)
                        <!-- Ribbon -->
                        <div class="ribbon"><span class="bg-primary">@lang('messages.wishlist')</span></div>
                    @endif

                    <!-- Product image -->
                    <div class="product-image">
                        <img class="card-img-top p-3" data-src="{{ $product->picture }}" src="{{ $product->picture }}"
                             data-holder-rendered="true">
                    </div>

                    <!-- Product body -->
                    <div class="card-body">
                        <p class="card-text product-name">{{ $product->name }}</p>

                        <div class="d-flex justify-content-between align-items-center">
                            <div class="form-parent">
                                @if(Auth::check())
                                    <!-- Wishlist add button -->
                                    <div class="form-add" {!! ($product->in_wishlist) ? "style='display: none;'" : "" !!}>
                                        {!! Form::open(['url' => route('add_wish')]) !!}
                                            {!! Form::hidden('user_id', Auth::user()->id) !!}
                                            {!! Form::hidden('product_id', $product->id) !!}
                                            <button type="submit" class="btn btn-sm btn-outline-success btn-add">
                                                <i class="fas fa-plus mr-1"></i>@lang('messages.wishlist_add')
                                            </button>
                                        {!! Form::close() !!}
                                    </div>
                                    <!-- Wishlist remove button -->
                                    <div class="form-remove"{!! ($product->in_wishlist) ? "" : "style='display: none;'" !!}>
                                        {!! Form::open(['url' => route('remove_wish')]) !!}
                                            {!! Form::hidden('user_id', Auth::user()->id) !!}
                                            {!! Form::hidden('product_id', $product->id) !!}
                                            <button type="submit" class="btn btn-sm btn-outline-danger btn-remove">
                                                <i class="fas fa-minus mr-1"></i>@lang('messages.wishlist_remove')
                                            </button>
                                        {!! Form::close() !!}
                                    </div>
                                @endif
                            </div>
                            <!-- Price -->
                            <h5 class="m-0 font-weight-bold text-primary">€{{ $product->price }}</h5>
                        </div>
                    </div>
                </div>

            </div>

        @endforeach

    </div>
    <!-- -- End Products zone -->

    <script>
        $(document).ready(function () {
            // Setting same height for all cards to display clean
            $('.product-name').matchHeight();
            $('.product-image').matchHeight();

            // Sort icons
            var icon1 = "<i class=\"fas fa-sort-alpha-down mr-2\"></i>";
            var icon2 = "<i class=\"fas fa-sort-alpha-up mr-2\"></i>";
            var icon3 = "<i class=\"fas fa-sort-numeric-down mr-2\"></i>";
            var icon4 = "<i class=\"fas fa-sort-numeric-up mr-2\"></i>";

            // Setup sort buttons at refresh
            switch ('{{ $sort }}') {
                case 'mark-asc':
                    console.log('mark-asc');
                    $("#sort-alpha").siblings('i').replaceWith(icon1);
                    $("#sort-alpha").attr('name', 'mark-asc');
                    $("#sort-alpha").parent().addClass('active');
                    break;
                case 'mark-desc':
                    console.log('mark-desc');
                    $("#sort-alpha").siblings('i').replaceWith(icon2);
                    $("#sort-alpha").attr('name', 'mark-desc');
                    $("#sort-alpha").parent().addClass('active');
                    break;
                case 'price-asc':
                default:
                    console.log('price-asc');
                    $("#sort-numeric").siblings('i').replaceWith(icon3);
                    $("#sort-numeric").attr('name', 'price-asc');
                    $("#sort-numeric").parent().addClass('active');
                    break;
                case 'price-desc':
                    console.log('price-desc');
                    $("#sort-numeric").siblings('i').replaceWith(icon4);
                    $("#sort-numeric").attr('name', 'price-desc');
                    $("#sort-numeric").parent().addClass('active');
                    break;
            }

            // Sorting button click
            $(".btn-sort").click(function (e) {
                var type = $(this).find("input").attr('name');
                switch (type) {
                    case 'mark-asc': // mark
                    case 'mark-desc':
                        if ($(this).hasClass('active')) { //already selected -> change order
                            if ($(this).find('input').attr('name') === 'mark-asc') {
                                $(this).find('i').replaceWith(icon2);
                                $(this).find('input').attr('name', 'mark-desc');
                            } else {
                                $(this).find('i').replaceWith(icon1);
                                $(this).find('input').attr('name', 'mark-asc');
                            }
                        } else { // not selected -> use displayed order
                            $(this).siblings().removeClass('active');
                            $(this).addClass('active');
                        }
                        break;
                    case 'price-asc': // price
                    case 'price-desc':
                        if ($(this).hasClass('active')) { //already selected -> change order
                            if ($(this).find('input').attr('name') === 'price-asc') {
                                $(this).find('i').replaceWith(icon4);
                                $(this).find('input').attr('name', 'price-desc');
                            } else {
                                $(this).find('i').replaceWith(icon3);
                                $(this).find('input').attr('name', 'price-asc');
                            }
                        } else { // not selected -> use displayed order
                            $(this).siblings().removeClass('active');
                            $(this).addClass('active');
                        }
                        break;
                }
                // Redirect url with sort param
                window.location.href = '{{route("home")}}/' + $(this).find('input').attr('name');
            });

            // Add to wishlist button click
            $(".btn-add").click(function (e) {
                e.preventDefault();
                var btn = $(this);
                // Form values
                var _token = btn.siblings("input[name='_token']").val(); // get csrf field
                var product_id = btn.siblings("input[name='product_id']").val();
                var user_id = btn.siblings("input[name='user_id']").val();
                $.ajax({
                    url: "{{ route('add_wish') }}",
                    type: 'POST',
                    data: {
                        _token: _token,
                        product_id: product_id,
                        user_id: user_id
                    },
                    success: function (data) {
                        // If success
                        if ($.isEmptyObject(data.error)) {
                            btn.closest(".form-parent").find(".form-add").hide();
                            btn.closest(".form-parent").find(".form-remove").show();
                        } else { // If error
                            alert(data.error);
                        }
                    }
                });
            });

            // Remove from wishlist button click
            $(".btn-remove").click(function (e) {
                e.preventDefault();
                var btn = $(this);
                // Form values
                var _token = btn.siblings("input[name='_token']").val(); // get csrf field
                var product_id = btn.siblings("input[name='product_id']").val();
                var user_id = btn.siblings("input[name='user_id']").val();
                $.ajax({
                    url: "{{ route('remove_wish') }}",
                    type: 'POST',
                    data: {
                        _token: _token,
                        product_id: product_id,
                        user_id: user_id
                    },
                    success: function (data) {
                        // If success
                        if ($.isEmptyObject(data.error)) {
                            // Switch button from remove to add
                            btn.closest(".form-parent").find(".form-remove").hide();
                            btn.closest(".form-parent").find(".form-add").show();
                        } else { // If error
                            alert(data.error);
                        }
                    }
                });
            });
        });
    </script>

@endsection