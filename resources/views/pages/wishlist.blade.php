@extends('layouts.default')

@section('content')

    <h1 class="display-4">@lang('messages.wishlist')</h1>

    <!-- Info if no wishes -->
    <div id="info-parent" class="text-center text-muted mt-5" style="{{ $wishes->isEmpty() ? "" : "display: none;" }}">
        <i class="fas fa-5x fa-th-large"></i>
        <h4 class="mt-3">@lang('messages.wishlist_empty')</h4>
        <div>
            {!! __('messages.wishlist_info', ['BUTTON' => "<span class='font-weight-bold'><i class='fas fa-plus mr-1'></i>".__('messages.wishlist_add')."</span>"]) !!}
        </div>
    </div>
    <!-- -- End Info if no wishes -->

    <!-- Wishes zone -->
    <div id="grid-parent" class="row" style="{{ $wishes->isEmpty() ? "display: none;" : "" }}">

        @foreach($wishes as $wish)

            <div class="col-sm-12 col-md-6 wish-parent">

                <div class="card mb-4 shadow-sm">
                    <div class="row">
                        <div class="col-md-12 col-lg-4">
                            <!-- Wish image -->
                            <img class="card-img-top p-3" data-src="{{ $wish->product->picture }}"
                                 src="{{ $wish->product->picture }}" data-holder-rendered="true">
                        </div>
                        <div class="col-md-12 col-lg-8">
                            <!-- Wish body -->
                            <div class="card-body">
                                <p class="card-text">{{ $wish->product->name }}</p>
                                <h4 class="font-weight-bold text-primary">€{{ $wish->product->price }}</h4>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="card-footer bg-transparent">
                                <div class="d-flex justify-content-between align-items-center">
                                    <!-- Wish date -->
                                    <small class="text-muted"><i class="far fa-calendar-plus mr-1"></i>
                                        {{ date("F j, Y, g:i a", strtotime($wish->updated_at)) }}
                                    </small>
                                    <!-- Wish remove button -->
                                    {!! Form::open(['url' => route('remove_wish')]) !!}
                                        {!! Form::hidden('user_id', Auth::user()->id) !!}
                                        {!! Form::hidden('product_id', $wish->product->id) !!}
                                        <button type="submit" class="btn btn-sm btn-outline-danger btn-remove">
                                            <i class="fas fa-minus mr-1"></i>@lang('messages.wishlist_remove')
                                        </button>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        @endforeach

    </div>
    <!-- -- End Wishes zone -->

    <script>
        $(document).ready(function () {

            // Remove wish button
            $(".btn-remove").click(function (e) {
                e.preventDefault();
                var btn = $(this);
                // Get form fields
                var _token = btn.siblings("input[name='_token']").val(); // get csrf field.
                var product_id = btn.siblings("input[name='product_id']").val();
                var user_id = btn.siblings("input[name='user_id']").val();
                $.ajax({
                    url: "{{ route('remove_wish') }}",
                    type: 'POST',
                    data: {
                        _token: _token,
                        product_id: product_id,
                        user_id: user_id
                    },
                    success: function (data) {
                        // If success
                        if ($.isEmptyObject(data.error)) {
                            // Remove clicked wish
                            btn.closest(".wish-parent").fadeOut("fast", function () {
                                $(this).remove();
                                // If no more wishes -> show info zone
                                if ($('.wish-parent').length === 0) {
                                    $("#grid-parent").hide();
                                    $("#info-parent").show();
                                }
                            });
                        } else { // If error
                            alert(data.error);
                        }
                    }
                });
            });
        });
    </script>

@endsection