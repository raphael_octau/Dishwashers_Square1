<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Simple wishlist management for an e-shop website that sells dishwashers">
<meta name="author" content="Raphael OCTAU">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="icon" href="images/favicon.ico"/>

<title>@lang('messages.dishwashers')</title>

<!-- CSS -->
<!-- Including Bootstrap -->
<link rel="stylesheet" href="css/app.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css"
      integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

<!-- JavaScript -->
<!-- Including Bootstrap and Jquery -->
<script src="js/app.js" type="text/javascript"></script>
<script src="js/jquery.matchHeight.js" type="text/javascript"></script>