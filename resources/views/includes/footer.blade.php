<footer class="footer bg-white">
    <div class="container text-center">
        <span class="text-muted">Copyright ©
            <a href="http://raphael.octau.dev.free.fr/?lang=en" data-toggle="tooltip" data-placement="top"
               title="{{ __('messages.visit_portfolio') }}">
                Raphaël OCTAU
            </a> 2018
        </span>
    </div>
</footer>