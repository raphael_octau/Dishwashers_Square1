<nav class="navbar navbar-expand-lg navbar-light bg-white fixed-top shadow-sm">
    <div class="container">
        <!-- Brand button -->
        <a class="navbar-brand mb-0 h1" href="{{ route('home') }}">
            <img src="images/dishwasher.svg" width="30" height="30" class="d-inline-block align-top">
            @lang('messages.dishwashers')
        </a>

        <!-- Mobile menu switch button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Desktop menu buttons -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <!-- Home button -->
                <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
                    <a class="nav-link" href="{{ route('home') }}">@lang('messages.home')</a>
                </li>
                <!-- Wishlist button -->
                <li class="nav-item {{ Request::is('wishlist') ? 'active' : '' }}">
                    @if(Auth::check())
                        <a class="nav-link" href="{{ route('wishlist') }}">@lang('messages.wishlist')</a>
                    @else
                        <span class="nav-link disabled" data-toggle="tooltip" data-placement="bottom"
                              title="{{ __('messages.wishlist_connect') }}">
                            @lang('messages.wishlist')
                        </span>
                    @endif
                </li>
            </ul>
            @if(!Auth::check())
                <!-- Login/Register buttons -->
                <a class="btn btn-link nav-link" href="{{ route('login') }}">@lang('messages.signin')</a>
                <a class="btn btn-sm btn-primary nav-link" href="{{ route('register') }}">@lang('messages.signup')</a>
            @else
                <!-- Logoff button -->
                <span class="nav-link d-none d-lg-block">
                    {{ isset(Auth::user()->name) ? __('messages.connected_as', ['USERNAME' => Auth::user()->name]) : __('messages.connected_as', ['USERNAME' => Auth::user()->email]) }}
                </span>
                <a class="btn btn-sm btn-danger nav-link" href="{{ route('logout') }}">@lang('messages.signout')</a>
            @endif
        </div>
    </div>
</nav>