<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body class="bg-light">

@include('includes.header')

<main role="main" class="container">

    @yield('content')

</main>

@include('includes.footer')

<script type="text/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
</body>
</html>