<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global Language Lines
    |--------------------------------------------------------------------------
    */

    // Home and global
    'dishwashers' => 'Dishwashers',
    'home' => 'Home',
    'wishlist' => 'Wishlist',
    'signin' => 'Log In',
    'signup' => 'Register',
    'signout' => 'Log Off',
    'connected_as' => 'Connected as : :USERNAME',
    'visit_portfolio' => 'Visit my portfolio',
    'mark' => 'Mark',
    'price' => 'Price',

    // Wishlist
    'wishlist_add' => 'Add to wishlist',
    'wishlist_remove' => 'Remove from wishlist',
    'wishlist_empty' => 'Empty wishlist',
    'wishlist_info' => 'Click :BUTTON on home page to fill the wish-list',
    'wishlist_connect' => 'You must be logged-in to use the wish-list',

    'an_error_occured' => 'An error occured',
    'wishlist_add_success' => 'Product successfully added to the wish-list',
    'wishlist_remove_success' => 'Product successfully removed the wish-list',
    'wishlist_add_duplicate' => 'Product already in wish-list',

];
