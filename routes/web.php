<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout',
]);

// Home route
Route::get('/{sort?}', [
    'as' => 'home',
    'uses' => 'HomeController@index',
])->where('sort', 'price-asc|price-desc|mark-asc|mark-desc');

// Wishlist route
Route::get('/wishlist', [
    'as' => 'wishlist',
    'uses' => 'WishesController@index',
]);

// Manage wish routes
Route::post('add_wish', ['as' => 'add_wish', 'uses' => 'WishesController@add']);
Route::post('remove_wish', ['as' => 'remove_wish', 'uses' => 'WishesController@remove']);