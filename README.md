[![License](https://img.shields.io/badge/license-CC%20BY--NC--ND%204.0-blue.svg)](LICENSE)

## About the project

This project is a web app in Laravel with Bootstrap to manage a wishlist of an e-shop that sells dishwashers.
In order to save their wishlist, the users must be logged-in.

To install the project :

1. Run `composer install` on the root of the project to create the `vendor` folder.
2. Create a `.env` file in the root of the project with values as in the `.env.example` file to setup your database.
3. After creating your database, run migrations with `php artisan migrate` on the root of the project to create the database tables.
4. Run seeds with `php artisan db:seed` then `php artisan db:seed --class=ProductsTableSeeder` on the root of the project to fill the database.
5. The index of the project is located in the `public` folder.

> I decided to use the seeds to fill database instead of looking for values directly from the [appliancesdelivered.ie](https://www.appliancesdelivered.ie/dishwashers) HTML page with [Goutte](https://github.com/dweidner/laravel-goutte) for ease of code design.
Indeed, appliancesdelivered.ie does not have a unique ID system visible in the HTML which does not allow me to spot wich product have changed on the website in the future (e.g. price change would move the position of the product in the list of appliancesdelivered.ie page sorted by price). So the Goutte should be done once, wich is the same as using seeds.

For more info, read the full README.

To see my other projects, go to my [portfolio](https://raphael-octau.fr/en).

## TODOs

- [ ] Using uniqid in place of simple int in database
- [ ] Adding more languages
- [ ] Adding more infos on products
- [ ] Custom error page instead of alert popup

## License

&copy; 2019 Raphaël OCTAU

This work is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](LICENSE).
