<?php

namespace App\Http\Controllers;

use App\Product;
use App\Wish;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Returns the index view with an optional sorting
     * @param null $sort the sorting type (price-asc, price-desc, mark-asc, mark-desc)
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($sort = null)
    {
        // If sorting type given (sorting string structure given in route)
        if ($sort) {
            $sort_type = explode("-", $sort)[0]; // column
            $sort_order = explode("-", $sort)[1]; // order
            $products = Product::orderBy($sort_type, $sort_order)->get();
        } else {
            $products = Product::get();
        }

        // If user authenticated, we list all of their wishes
        if (Auth::check()) {
            // Get all wishes products for this user
            $wishes = Wish::select('product_id')->where('user_id', Auth::user()->id)->get()->toArray();
            $wishes = array_column($wishes, 'product_id');

            // Set in_wishlist? for each products
            foreach ($products as $product) {
                $product->in_wishlist = in_array($product->id, $wishes);
            }
        }

        return view('pages/index', compact('products', 'sort'));
    }
}
