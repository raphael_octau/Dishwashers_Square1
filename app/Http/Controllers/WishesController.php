<?php

namespace App\Http\Controllers;

use App\Product;
use App\Wish;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;

class WishesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Returns the index view with all wishes for this user
     * Authentication required in construct
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $wishes = Wish::where('user_id', Auth::user()->id)->get();
        return view('pages/wishlist', compact('wishes'));
    }

    /**
     * Insert a wish in database from product_id and user_id
     * @param Request $request product_id and user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(Request $request)
    {
        // Checking if user_id field is the same as real user_id for security
        if (Auth::user()->id != $request->user_id) {
            return response()->json(['error' => __('messages.an_error_occured')]);
        }

        // Inserts only if wish doesn't exists
        if (!Wish::where([['product_id', $request->product_id], ['user_id', $request->user_id]])->first()) {

            try { // If form values are changed by user, for security
                Wish::create($request->all());
                return response()->json(['success' => __('messages.wishlist_add_success')]);
            } catch (\Exception $e) {
                return response()->json(['error' => __('messages.an_error_occured')]);
            }

        } else {
            return response()->json(['error' => __('messages.wishlist_add_duplicate')]);
        }
    }

    /**
     * Removes as wish in database from product_id and user_id
     * @param Request $request product_id and user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function remove(Request $request)
    {
        // Checking if user_id field is the same as real user_id for security
        if (Auth::user()->id != $request->user_id) {
            return response()->json(['error' => __('messages.an_error_occured')]);
        }

        try { // If form values are changed by user, for security
            $wish = Wish::where([['product_id', $request->product_id], ['user_id', $request->user_id]])->first();
            Wish::destroy($wish->id);
            return response()->json(['success' => __('messages.wishlist_remove_success')]);
        } catch (\Exception $e) {
            return response()->json(['error' => __('messages.an_error_occured')]);
        }
    }
}
